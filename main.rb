require "rubygems"
require 'sinatra'
require 'sinatra/reloader'
require 'active_record'
require 'rss'

ActiveRecord::Base.configurations = YAML.load_file('database.yml')
ActiveRecord::Base.establish_connection('development')

class InitialSchema < ActiveRecord::Migration

  def change
    unless table_exists? :cdinfos
      create_table :cdinfos do |t|
        t.string :title
        t.string :artist
        t.text :description
        t.integer :score
      end
    end
  end
end

InitialSchema.migrate(:change)

class Cdinfo < ActiveRecord::Base
end

get '/url' do
@rss = RSS::Parser.parse("http://matome.naver.jp/feed/topic/1LvAg")
erb :url
end
get '/' do
@title = "CDデータ管理システム"
  erb :index
end

get '/list' do
  @title = "CDデータの表示"
  erb :list
end

post '/list' do
  @title = "CDデータの表示"
end

get '/entry' do
  @title = "CDデータの登録"
  erb :entry
end

post '/entry' do
  cdinfo = Cdinfo.new
  cdinfo.title = params[:title]
  cdinfo.artist = params[:artist]
  cdinfo.description = params[:description]
  cdinfo.score = params[:score]
  cdinfo.save
  erb :entered
end


get '/retrieve' do
  @title = "CDデータの検索"
  erb :retrieve
end

get '/retrieve_title' do
  @title = "CD名で検索"
  erb :retrieve_title
end

post '/retrieve_title' do
  @title_results = Cdinfo.where("lower(title) like ?" , "%"+params[:title_result]+"%")
 erb :title_result
end

get '/retrieve_artist' do
  @title = "アーティスト名で検索"
  erb :retrieve_artist
end

post '/retrieve_artist' do
  @artist_results = Cdinfo.where("lower(artist) like ?" , "%"+params[:artist_result]+"%")
  erb :artist_result
end

delete '/del' do
  del= Cdinfo.find(params[:id])
  del.destroy
  erb :delete
end

post '/edit' do
 @edits=Cdinfo.where(id: params[:id])
 erb :edit
 end

post '/edit2' do
  info = Cdinfo.find(params[:id])
  info.title = params[:title]
  info.artist = params[:artist]
  info.description = params[:description]
  info.score = params[:score]
  info.save
  erb :edit2
end

